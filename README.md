# libasm

Familiarisation avec le langage assembleur.

## A propos

Comprendre le fonctionnement des registres, de la pile, des appels systèmes et de la mémoire.

Coder les fonctions: strlen, strcpy, strcmp, write et read (en utilisant syscall).

Lire le [sujet][1]

`Ce projet à été codé pour MACOS et Linux`

### Compiler et lancer le projet

1. Télécharger / Cloner le repo

```
git clone https://gitlab.com/flmarsil42/libasm.git
```

2. `cd` dans le dossier racine, et lancer `make`

```
cd libasm
make
```

3.  compiler votre `main.c` ou le `main.c` dans le repo avec `libasm.a` 
    
## Sources

- [Langage assembleur (FR)][2]
- [Assembly language][4]
- [Nasm tutorial][8]
- [Using stack : push and pop][3]
- [Flags register][5]
- [Introduction aux buffer overflows (FR)][6]
- [Introduction à la rétroingénierie des binaires (FR)][7]

[1]: https://gitlab.com/flmarsil42/libasm/-/blob/master/fr.subject.pdf
[2]: https://www.lacl.fr/tan/asm
[3]: https://stackoverflow.com/questions/13091987/x64-nasm-pushing-memory-addresses-onto-the-stack-call-function
[4]: https://software.intel.com/content/www/us/en/develop/articles/introduction-to-x64-assembly.html
[5]: https://www.youtube.com/watch?v=oQKa5q-jVzY&ab_channel=BeNew
[6]: https://zestedesavoir.com/articles/100/introduction-aux-buffer-overflows/
[7]: https://zestedesavoir.com/articles/97/introduction-a-la-retroingenierie-de-binaires/#fn-6-e1__RVmdu
[8]: https://cs.lmu.edu/~ray/notes/nasmtutorial/
