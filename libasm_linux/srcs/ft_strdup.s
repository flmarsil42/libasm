segment .text
global ft_strdup

extern ft_strlen
extern ft_strcpy
extern malloc

;char *strdup(const char *s);
;char *strdup(rdi);

;void *malloc(size_t size);
;void *malloc(rdi);

ft_strdup:
			push 	rdi
			call 	ft_strlen

			mov 	rdi, rax
			call 	malloc
			push 	rax

			pop 	rdi
			mov 	rsi, rdi
			mov 	rdi, rax

			call 	ft_strcpy

			pop 	rax
			ret