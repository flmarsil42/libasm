segment .text
extern	__errno_location
global ft_strcpy

ft_strcpy:
			xor		rdx, rdx
			xor		rcx, rcx
			cmp 	rsi, 0
			jz		end
			cmp 	rdi, 0
			jz		end
			jmp 	copy

incr:
			inc 	rcx

copy:
			mov		dl, BYTE [rsi + rcx]
			mov		BYTE [rdi + rcx], dl
			cmp		dl, 0
			jnz 	incr

end:
			mov		rax, rdi
			ret