segment .text
extern __errno_location
global ft_read

;size_t read(unsigned int fd, char *buf, size_t count);
;size_t read(rdi, rsi, rdx);
;rax = 0x00

ft_read:
			mov 	rax, 0x0
			syscall
			cmp		rax, 0
			jge 	end

error:
			neg		rax
			push 	rax
			call 	__errno_location
			mov 	r10, rax
			pop 	rax
			mov 	[r10], rax
			mov 	rax, -1

end:
			ret
