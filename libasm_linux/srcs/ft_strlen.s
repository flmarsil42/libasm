segment .text
extern	__errno_location
global ft_strlen

ft_strlen:
			xor		rax, rax
			jmp		comp

incr:
			inc 	rax

comp:
			cmp		BYTE [rdi + rax], 0
			jne		incr

end:
			ret