segment .text
global _ft_strdup

extern _ft_strlen
extern _ft_strcpy
extern _malloc

;char *strdup(const char *s);
;char *strdup(rdi);

;void *malloc(size_t size);
;void *malloc(rdi);

_ft_strdup:
			push 	rdi
			call 	_ft_strlen

			mov 	rdi, rax
			call 	_malloc
			push 	rax

			pop 	rdi
			mov 	rsi, rdi
			mov 	rdi, rax

			call 	_ft_strcpy

			pop 	rax
			ret