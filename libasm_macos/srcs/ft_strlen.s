segment .text
extern	___error
global _ft_strlen

_ft_strlen:
			xor		rax, rax
			jmp		comp

incr:
			inc 	rax

comp:
			cmp		BYTE [rdi + rax], 0
			jne		incr

end:
			ret