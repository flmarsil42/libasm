segment .text
extern	___error
global _ft_write

; int ft_write(int rdi, const void *rsi, size_t rdx);

_ft_write:
    		mov 	rax, 0x2000004
			syscall
			jnc 	end

error:
			push 	rax
			call 	___error
			mov 	r10, rax
			pop 	rax
			mov 	[r10], rax
			mov 	rax, -1

end:
			ret
