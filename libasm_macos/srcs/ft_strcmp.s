segment .text
extern	___error
global _ft_strcmp

_ft_strcmp:
			xor		rcx, rcx
			jmp		comp

ifzero:
			cmp		BYTE [rdi + rcx], 0 	; verification si \0
			jz 		end
			inc 	rcx

comp:
			mov 	dl, BYTE [rdi + rcx]
			mov		al, BYTE [rsi + rcx]
			sub 	dl, al
			jnz		end
			jz 		ifzero

end:
			mov 	al, dl
			ret
		