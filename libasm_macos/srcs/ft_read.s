segment .text
extern ___error
global _ft_read

;size_t read(unsigned int fd, char *buf, size_t count);
;size_t read(rdi, rsi, rdx);
;rax = 0x00

_ft_read:
			mov		rax, 0x2000003
			syscall
			jnc 	end

error:
			push 	rax
			call 	___error
			mov 	r10, rax
			pop 	rax
			mov 	[r10], rax
			mov 	rax, -1

end:
			ret
